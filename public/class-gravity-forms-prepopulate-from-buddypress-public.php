<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/public
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Gravity_Forms_Prepopulate_From_Buddypress_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $gravity_forms_prepopulate_from_buddypress    The ID of this plugin.
	 */
	private $gravity_forms_prepopulate_from_buddypress;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $gravity_forms_prepopulate_from_buddypress       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $gravity_forms_prepopulate_from_buddypress, $version ) {

		$this->gravity_forms_prepopulate_from_buddypress = $gravity_forms_prepopulate_from_buddypress;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Prepopulate_From_Buddypress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Prepopulate_From_Buddypress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->gravity_forms_prepopulate_from_buddypress, plugin_dir_url( __FILE__ ) . 'css/gravity-forms-prepopulate-from-buddypress-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Prepopulate_From_Buddypress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Prepopulate_From_Buddypress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->gravity_forms_prepopulate_from_buddypress, plugin_dir_url( __FILE__ ) . 'js/gravity-forms-prepopulate-from-buddypress-public.js', array( 'jquery' ), $this->version, false );

	}

  public function populate_buddypress_fields( $form ) {

    if ( is_user_logged_in() !== TRUE ) return $form;
    
    foreach ( $form['fields'] as &$field ) {
      
      if ( $field->allowsPrepopulate !== TRUE ) continue;
      if ( strpos($field->inputName,'buddypress_') !== 0 ) continue;
      
      add_filter( 'gform_field_value_' . $field->inputName, array( $this, 'buddypress_field_value' ), 10, 3 );
      
    }
    return $form;
    
  }
  
  public function buddypress_field_value( $value, $field, $tmp_field_name ) {
    
    $field_name = str_replace( '_', ' ', $this->strip_prefix( 'buddypress_', $tmp_field_name ));    
    $value = xprofile_get_field_data( $field_name, wp_get_current_user()->ID );
    return $value;
    
  }

  private function strip_prefix( $prefix, $str ) {
    if (substr($str, 0, strlen($prefix)) == $prefix) {
      $str = substr($str, strlen($prefix));
    }
    return $str;
  }
  
}
