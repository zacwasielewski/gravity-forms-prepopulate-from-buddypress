<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wasielewski.org
 * @since             0.0.1
 * @package           Gravity_Forms_Prepopulate_From_Buddypress
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms prepopulate from BuddyPress
 * Plugin URI:        https://bitbucket.org/zacwasielewski/gravity-forms-prepopulate-from-buddypress
 * Description:       Use buddypress_{field name} in Gravity Forms to prepopulate field with current user's BuddyPress data.
 * Version:           0.0.1
 * Author:            Zac Wasielewski
 * Author URI:        http://wasielewski.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gravity-forms-prepopulate-from-buddypress
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gravity-forms-prepopulate-from-buddypress-activator.php
 */
function activate_gravity_forms_prepopulate_from_buddypress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-prepopulate-from-buddypress-activator.php';
	Gravity_Forms_Prepopulate_From_Buddypress_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gravity-forms-prepopulate-from-buddypress-deactivator.php
 */
function deactivate_gravity_forms_prepopulate_from_buddypress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-prepopulate-from-buddypress-deactivator.php';
	Gravity_Forms_Prepopulate_From_Buddypress_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gravity_forms_prepopulate_from_buddypress' );
register_deactivation_hook( __FILE__, 'deactivate_gravity_forms_prepopulate_from_buddypress' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-prepopulate-from-buddypress.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gravity_forms_prepopulate_from_buddypress() {

	$plugin = new Gravity_Forms_Prepopulate_From_Buddypress();
	$plugin->run();

}
run_gravity_forms_prepopulate_from_buddypress();
