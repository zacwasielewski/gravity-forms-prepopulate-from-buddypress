<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Gravity_Forms_Prepopulate_From_Buddypress_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
