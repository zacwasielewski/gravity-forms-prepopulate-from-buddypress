<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gravity_Forms_Prepopulate_From_Buddypress
 * @subpackage Gravity_Forms_Prepopulate_From_Buddypress/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Gravity_Forms_Prepopulate_From_Buddypress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
